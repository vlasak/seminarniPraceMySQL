//IMPORTY
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//INICIALIZACE ROUTES
var index = require('./routes/index');
var sestavy = require('./routes/sestavy');
var sestavy2 = require('./routes/sestavy2');
var sestavy3 = require('./routes/sestavy3');
var sestavy3pre = require('./routes/sestavy3pre');
var sestavy4 = require('./routes/sestavy4');
var forms = require('./routes/forms');
var forms2 = require('./routes/forms2');
var code = require('./routes/code');
var datab = require('./routes/datab');
var otazky = require('./routes/otazky');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/sestavy', sestavy);
app.use('/sestavy2', sestavy2);
app.use('/sestavy3', sestavy3);
app.use('/sestavy3pre', sestavy3pre);
app.use('/sestavy4', sestavy4);
app.use('/forms', forms);
app.use('/forms2', forms2);
app.use('/datab', datab);
app.use('/code', code);
app.use('/otazky', otazky);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('page/error');
});

module.exports = app;
