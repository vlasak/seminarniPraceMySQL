# Seminární práce z IKT



#### Projekt je vypracován v Node JS a MySQL


## Základní Princip:
Uživatel zadá adresu serveru
Server se zeptá co chce uživatel zobrazit
Na výběr budou pohledy, a formuláře k zadání záznamů do databáze


## Cíle:

#### Dazabáze
    
- [x] Vytvořit MySQL databázi
    - [x] Vytvořit Tabulky
    - [x] Vytovřit Relace
    - [ ] Vytvořit Dotazy
    - [x] Vytvořit Views


#### Node js Server
- [x] Připojení na databázi (MySQL)
    - [x] Komunikace s DB
    - [x] Zpracování dat z MySQL
- [x] Vykreslování dat na stránky
- [x] Vytvoření formuláře pro přístup do MySQL
- [ ] Blbuvzdorný //Víceméně
- [x] Dostupný na veřejné IP
