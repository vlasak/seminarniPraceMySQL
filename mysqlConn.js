const mysql = require('mysql');
var ress = undefined;
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'flexbot',
  password : '',
  database : 'seminarka'
});
connection.connect(function (err) {
  if(err == null){
    console.log(new Date()+ ': Připojení k databázi bylo úspěšné');
  }
  else{
    console.log(err);
  }
});

module.exports = {
  quarySelect: function(sql, callback) {
    connection.query(sql,function (error, results, fields) {
      if (error) throw error;
      ress = results;
      callback(ress);
    });
  },
  quaryInsert: function (sql, callback, data) {
    connection.query(sql, data, function (error, results, fields) {
      if (error) throw error;
      ress = results;
      callback(ress);
    });
  },
  quarySelWhere: function (sql, callback, data) {
    connection.query(sql,data,function (error, results, fields) {
      if (error) throw error;
      ress = results;
      callback(ress);
    });
  },
  ressReturn: function () {
    return ress;
  }
};
