var express = require('express');
var router = express.Router();

//Get API page
router.get('/', function(req,res,next) {
  res.render('page/code');
});

module.exports = router;
