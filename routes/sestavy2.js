var express = require('express');
var router = express.Router();
const mysql = require('mysql');
const mysqlConn = require('../mysqlConn');
var tempRes = undefined;

router.get('/', function(req,res,next) {
  tempRes = res;
  mysqlConn.quarySelect('SELECT * FROM `display_uziv_soft` ORDER BY `display_uziv_soft`.`prijmeni` ASC', vyrenderujSestavu);
});

function vyrenderujSestavu() {
  tempRes.render('page/sestavy2',{result: mysqlConn.ressReturn()});
}
module.exports = router;
