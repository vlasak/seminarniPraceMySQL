var express = require('express');
var router = express.Router();
const mysqlConn = require('../mysqlConn');
var tempRes = undefined;
var tempPost = undefined;

router.get('/', function(req,res,next) {
  tempRes = res;
  mysqlConn.quarySelect('SELECT * FROM `display_uziv_zariz` ORDER BY `display_uziv_zariz`.`prijmeni` ASC', vyrenderujSestavu);
});

router.post('/', function(req,res,next){
  tempRes = res;
  tempPost  = {id_uziv: req.body.vyberUziv, id_soft: req.body.vyberSoft};
  mysqlConn.quarySelect('SELECT * FROM `spojovaci`', overHod)
});

function vyrenderujSestavu() {
  tempRes.render('page/forms2',{state: 'unsuccess',result: mysqlConn.ressReturn()});
}
function vyrenderujPreForm() {
  mysqlConn.quarySelect('SELECT * FROM `display_uziv_zariz` ORDER BY `display_uziv_zariz`.`prijmeni` ASC', vyrenderujForm);
}
function vyrenderujForm(){
  tempRes.render('page/forms2',{state: 'success', result: mysqlConn.ressReturn()});
}
function vyrenderujFormERROR(){
  tempRes.render('page/forms2',{state: 'error', result: mysqlConn.ressReturn()});
}

//Vyhodnocení duplicity
function overHod(SpojovaciDB) {
  let n = 0;

  for (var i = 0; i < SpojovaciDB.length; i++) {
    if (SpojovaciDB[i]['id_uziv'] == tempPost.id_uziv && SpojovaciDB[i]['id_soft'] == tempPost.id_soft) {
      n++;
    }
  }
  
  if (n >= 1) {
    mysqlConn.quarySelect('SELECT * FROM `display_uziv_zariz` ORDER BY `display_uziv_zariz`.`prijmeni` ASC',vyrenderujFormERROR);
  } else {
    mysqlConn.quaryInsert('INSERT INTO `spojovaci` SET ?', vyrenderujPreForm, tempPost);
  }
  console.log(n);
}

module.exports = router;
