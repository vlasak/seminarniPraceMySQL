var express = require('express');
var router = express.Router();
const mysql = require('mysql');
const mysqlConn = require('../mysqlConn');
var tempRes = undefined;

router.get('/', function(req,res,next) {
  tempRes = res;
  mysqlConn.quarySelect('SELECT * FROM `display_uziv_zariz`', vyrenderujSestavu);
});

function vyrenderujSestavu() {
  tempRes.render('page/sestavy3',{result: mysqlConn.ressReturn()});
}

module.exports = router;
